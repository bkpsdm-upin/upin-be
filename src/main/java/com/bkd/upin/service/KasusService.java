package com.bkd.upin.service;

import com.bkd.upin.model.TabelKasus;
import com.bkd.upin.model.TabelKelompokKasus;
import com.bkd.upin.model.TabelStatuspersonal;
import com.bkd.upin.model.TabelStatusproses;
import com.bkd.upin.model.param.ResponseReportHukdis;
import com.bkd.upin.payload.ApiResponse;
import com.bkd.upin.payload.PagedResponse;
import com.google.gson.JsonObject;
import net.minidev.json.JSONArray;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface KasusService {

    PagedResponse<TabelKelompokKasus> kelompokKasus(int page, int size);
    PagedResponse<TabelStatusproses> statusProses(int page, int size);
    PagedResponse<TabelStatuspersonal> statusPersonal(int page, int size);
    PagedResponse<TabelKasus> kasuses(TabelKasus param, int page, int size);
    ResponseEntity<List<ResponseReportHukdis>> reporting(TabelKasus kasus);
    ResponseEntity<TabelKasus> getKasus(Long id);
    ResponseEntity<ApiResponse> updateKasus(Long id, TabelKasus newKasus);
    ResponseEntity<ApiResponse> verifikasiKasus(TabelKasus kasus);
    ResponseEntity<ApiResponse> deleteKasus(Long id);
}
