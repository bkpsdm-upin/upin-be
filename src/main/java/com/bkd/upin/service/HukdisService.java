package com.bkd.upin.service;

import com.bkd.upin.model.Hukdis;
import com.bkd.upin.payload.ApiResponse;
import com.bkd.upin.payload.PagedResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.JsonObject;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface HukdisService {
    PagedResponse<Hukdis> allHukdis(int page, int size);
    ResponseEntity<List<Hukdis>> allHukdisByIdKasus(Long idKasus);
    ResponseEntity<ApiResponse> listHukdis(Long idKasus);
    ResponseEntity<Map<String, Object>> jenisHukdis();
    ResponseEntity<ApiResponse> updateHukdis(Hukdis hukdis);
    ResponseEntity<ApiResponse> delete(Long id);
}
