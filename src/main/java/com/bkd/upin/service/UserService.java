package com.bkd.upin.service;

import com.bkd.upin.model.SysdbPns;
import com.bkd.upin.payload.ApiResponse;
import org.springframework.http.ResponseEntity;

public interface UserService {
    ResponseEntity<ApiResponse> getByNip(String nip);
    ResponseEntity<ApiResponse> setRiwayatHukdis(Long nip, String kode);
}
