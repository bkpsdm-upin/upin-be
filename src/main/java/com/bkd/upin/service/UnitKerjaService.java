package com.bkd.upin.service;

import com.bkd.upin.model.UnitKerja;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UnitKerjaService {
    ResponseEntity<List<UnitKerja>> findByStatusAndInscod();
}
