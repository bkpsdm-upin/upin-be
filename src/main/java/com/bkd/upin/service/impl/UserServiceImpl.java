package com.bkd.upin.service.impl;

import com.bkd.upin.model.SysdbPns;
import com.bkd.upin.payload.ApiResponse;
import com.bkd.upin.repository.UserRepository;
import com.bkd.upin.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public ResponseEntity<ApiResponse> getByNip(String nip) {

        try {
            SysdbPns user = userRepository.findByNipBaru(nip);
            if(user != null) {
                ObjectMapper mapper = new ObjectMapper();
                return new ResponseEntity<>(new ApiResponse(Boolean.TRUE, "Sukses...",mapper.writeValueAsString(user)), HttpStatus.OK);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(new ApiResponse(Boolean.FALSE, "Data Not Found...", ""), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse> setRiwayatHukdis(Long nip, String kode) {
        userRepository.update(nip, kode);
        SysdbPns user = userRepository.findByNipBaru(nip.toString());
        if(user == null) {
            return new ResponseEntity<>(new ApiResponse(Boolean.FALSE, "Failed Updating Data...", ""), HttpStatus.OK);
        }
        return new ResponseEntity<>(new ApiResponse(Boolean.TRUE, "Success...", ""), HttpStatus.OK);
    }
}
