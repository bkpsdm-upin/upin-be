package com.bkd.upin.service.impl;

import com.bkd.upin.exception.ResourceNotFoundException;
import com.bkd.upin.model.*;
import com.bkd.upin.model.param.ResponseReportHukdis;
import com.bkd.upin.payload.ApiResponse;
import com.bkd.upin.payload.PagedResponse;
import com.bkd.upin.repository.*;
import com.bkd.upin.repository.spesification.KasusSpesifications;
import com.bkd.upin.service.KasusService;
import com.bkd.upin.utils.AppUtils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.bkd.upin.utils.AppConstants.ID;

@Service
public class KasusServiceImpl implements KasusService {
    private static final String KASUS_STR = "Kasus";
    private static final String YOU_DON_T_HAVE_PERMISSION_TO_MAKE_THIS_OPERATION = "You don't have permission to make this operation";


    @Autowired
    private KelompokKasusRepository kelompokKasusRepository;

    @Autowired
    private StatusProsesRepository statusProsesRepository;

    @Autowired
    private KasusRepository kasusRepository;

    @Autowired
    private StatusPersonalRepository statusPersonalRepository;

    @Autowired
    private HukdisRepository hukdisRepository;

    @Autowired
    private JenisHukdisRepository jenisHukdisRepository;

    @Override
    public PagedResponse<TabelKelompokKasus> kelompokKasus(int page, int size) {
        AppUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "idKelompokKasus");
        Page<TabelKelompokKasus> list = kelompokKasusRepository.findAll(pageable);
        List<TabelKelompokKasus> content = list.getNumberOfElements() == 0 ? Collections.emptyList() : list.getContent();
        return new PagedResponse<>(content, list.getNumber(), list.getSize(), list.getTotalElements(),
                list.getTotalPages(), list.isLast());
    }

    @Override
    public PagedResponse<TabelStatusproses> statusProses(int page, int size) {
        AppUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "idStatusProses");
        Page<TabelStatusproses> list = statusProsesRepository.findAll(pageable);
        List<TabelStatusproses> content = list.getNumberOfElements() == 0 ? Collections.emptyList() : list.getContent();
        return new PagedResponse<>(content, list.getNumber(), list.getSize(), list.getTotalElements(),
                list.getTotalPages(), list.isLast());
    }

    @Override
    public PagedResponse<TabelStatuspersonal> statusPersonal(int page, int size) {
        AppUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "idStatus");
        Page<TabelStatuspersonal> list = statusPersonalRepository.findAll(pageable);
        List<TabelStatuspersonal> content = list.getNumberOfElements() == 0 ? Collections.emptyList() : list.getContent();
        return new PagedResponse<>(content, list.getNumber(), list.getSize(), list.getTotalElements(),
                list.getTotalPages(), list.isLast());
    }

    @Override
    public PagedResponse<TabelKasus> kasuses(TabelKasus param, int page, int size) {
        AppUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "idKasus");
//        Page<TabelKasus> list = kasusRepository.findByStatusVerifikasi(status, pageable);
        Page<TabelKasus> list = kasusRepository.findAll(KasusSpesifications.findBySearchCreiteria(param), pageable);
        List<TabelKasus> content = list.getNumberOfElements() == 0 ? Collections.emptyList() : list.getContent();
        return new PagedResponse<>(content, list.getNumber(), list.getSize(), list.getTotalElements(),
                list.getTotalPages(), list.isLast());
    }

    @Override
    public ResponseEntity<List<ResponseReportHukdis>> reporting(TabelKasus kasus) {
        List<TabelKasus> list = kasusRepository.findAll(KasusSpesifications.findBySearchCreiteria(kasus));
        List<Hukdis> hukdisList = hukdisRepository.findAllByNipLamaNotNull();
//        System.out.println(hukdisList);
//        JSONArray array = new JSONArray();
        List<ResponseReportHukdis> res = new ArrayList<>();
        int no = 1;
        for (TabelKasus kas : list) {
            ResponseReportHukdis data = new ResponseReportHukdis();
            for (Hukdis hukdis : hukdisList) {
                if(kas.getIdKasus() == hukdis.getIdKasus()) {
                    data.setNo(no);
                    data.setNip(hukdis.getNip());
                    String name = (hukdis.getGlrDepan() == null ? "" : hukdis.getGlrDepan() +" ") + hukdis.getNama();
                    if(hukdis.getGlrBelakang() != null)
                        name += ", " + hukdis.getGlrBelakang();
                    data.setNama(name);
                    data.setPangkat(hukdis.getGolru().getPangkat());
                    data.setGolongan(hukdis.getGolru().getGolongan());
                    data.setJabatan(hukdis.getJabatan().getJabatan());
                    data.setUnitKerja(hukdis.getUnitKerja().getNama());
                    data.setKelompokKasus(kas.getKelompokKasus() == null ? null : kas.getKelompokKasus().getPpYangMengatur());
                    Optional<JenisHukdis> jenisHukdis = jenisHukdisRepository.findByKodeHukdis(hukdis.getPutusanHukdis());
                    if(jenisHukdis.isPresent())
                        data.setPutusan(jenisHukdis.get().getHukdis());
                    data.setNomorSk(hukdis.getNoSkPutusan());
                    data.setTanggalSk(hukdis.getTglSkPutusan());
                    data.setKeterangan(hukdis.getKeterangan());
                    data.setStatus(kas.getStatusProses() == null ? null : kas.getStatusProses().getStatusProses());


//                    obj.put("noSuratAduan", kas.getNoSuratAduan());
//                    obj.put("tanggalSuratAduan", kas.getTanggalSuratAduan());
//                    obj.put("kelompok", kas.getKelompokKasus().getPpYangMengatur());
//                    obj.put("statusProses", kas.getStatusProses().getStatusProses());
//                    obj.put("nip", hukdis.getNip());
//                    obj.put("nama", hukdis.getNama());
//                    obj.put("glrDepan", hukdis.getGlrDepan());
//                    obj.put("glrBelakang", hukdis.getGlrBelakang());
//                    obj.put("pangkat", hukdis.getGolru().getPangkat() + " / " + hukdis.getGolru().getGolongan());
//                    obj.put("jabatan", hukdis.getJabatan().getJabatan());
//                    obj.put("alamat", hukdis.getAlamat());
//                    obj.put("unitKerja", hukdis.getUnitKerja().getNama());
//                    obj.put("noSkPutusan", hukdis.getNoSkPutusan());
//                    obj.put("tglSkPutusan", hukdis.getTglSkPutusan());
//                    Optional<JenisHukdis> jenisHukdis = jenisHukdisRepository.findByKodeHukdis(hukdis.getPutusanHukdis());
//                    if(jenisHukdis.isPresent()) {
//                        obj.put("putusanHukdis", jenisHukdis.get().getHukdis());
//                    } else
//                        obj.put("putusanHukdis", null);
//                    obj.put("keterangan", hukdis.getKeterangan());
                }
            }
            no++;
            res.add(data);
        }

        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<TabelKasus> getKasus(Long id) {
        TabelKasus kasus = kasusRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(KASUS_STR, ID, id));
        return new ResponseEntity<>(kasus, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse> updateKasus(Long id, TabelKasus newKasus) {
        TabelKasus kasus = kasusRepository.save(newKasus);
        return new ResponseEntity<>(new ApiResponse(Boolean.TRUE, "Sukses...", new Gson().toJson(kasus)), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse> verifikasiKasus(TabelKasus kasus) {
        TabelKasus update = kasusRepository.save(kasus);
        return new ResponseEntity<>(new ApiResponse(Boolean.TRUE, "Sukses...", ""),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse> deleteKasus(Long id) {
        kasusRepository.deleteById(id);
        return new ResponseEntity<>(new ApiResponse(Boolean.TRUE, "Hapus Kasus Sukses...", ""), HttpStatus.OK);
    }
}
