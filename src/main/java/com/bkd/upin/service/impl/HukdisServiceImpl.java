package com.bkd.upin.service.impl;

import com.bkd.upin.exception.ApiException;
import com.bkd.upin.model.Hukdis;
import com.bkd.upin.model.JenisHukdis;
import com.bkd.upin.payload.ApiResponse;
import com.bkd.upin.payload.PagedResponse;
import com.bkd.upin.repository.HukdisRepository;
import com.bkd.upin.repository.JenisHukdisRepository;
import com.bkd.upin.service.HukdisService;
import com.bkd.upin.utils.AppUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class HukdisServiceImpl implements HukdisService {
    private static final String HUKDIS_STR = "Kasus";
    private static final String YOU_DON_T_HAVE_PERMISSION_TO_MAKE_THIS_OPERATION = "You don't have permission to make this operation";

    @Autowired
    private HukdisRepository hukdisRepository;
    @Autowired
    private JenisHukdisRepository jenisHukdisRepository;

    @Override
    public PagedResponse<Hukdis> allHukdis(int page, int size) {
        AppUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "idHukdis");
        Page<Hukdis> list = hukdisRepository.findAll(pageable);
        List<Hukdis> content = list.getNumberOfElements() == 0 ? Collections.emptyList() : list.getContent();
        return new PagedResponse<>(content, list.getNumber(), list.getSize(), list.getTotalElements(),
                list.getTotalPages(), list.isLast());
    }

    @Override
    public ResponseEntity<List<Hukdis>> allHukdisByIdKasus(Long idKasus) {
        List<Hukdis> content = hukdisRepository.findAllByIdKasus(idKasus);

        return new ResponseEntity<>(content, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse> listHukdis(Long idKasus) {
        try{
            List<Hukdis> content = hukdisRepository.findAllByIdKasus(idKasus);
            if(content.isEmpty()) {
                return new ResponseEntity<>(new ApiResponse(Boolean.FALSE, "Not Found...", ""), HttpStatus.OK);
            }

            JSONObject res = new JSONObject();
            JSONArray arrayTergugat = new JSONArray();
            JSONArray arrayPenggugat = new JSONArray();
            for (Hukdis hukdis : content) {
                if(hukdis.getStatusPersonal().getIdStatus().equals("01")) {
                    arrayTergugat.add(hukdis);
                } else
                    arrayPenggugat.add(hukdis);
            }
            res.put("tergugat", arrayTergugat);
            res.put("penggugat", arrayPenggugat);
            return new ResponseEntity<>(new ApiResponse(Boolean.TRUE, "Sukses...", new GsonBuilder().setDateFormat("yyyy-MM-dd").create().toJson(res)), HttpStatus.OK);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new ResponseEntity<>(new ApiResponse(Boolean.FALSE, "Error...", ""), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    public ResponseEntity<Map<String, Object>> jenisHukdis() {
        List<JenisHukdis> list = jenisHukdisRepository.findAll();
        if (list.size() == 0)
            throw new ApiException(HttpStatus.NOT_FOUND, "Data not Found...");
        Map<String, Object> response = new HashMap<>();
        response.put("content", list);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse> updateHukdis(Hukdis hukdis) {
        if(hukdis.getGolru().getId() == null
                || hukdis.getJabatan().getKdJabatan() == null) {
            hukdisRepository.insert(hukdis.getIdKasus(), hukdis.getNama(), hukdis.getAlamat(), hukdis.getKeterangan(), hukdis.getStatusPersonal().getIdStatus());
            return new ResponseEntity<>(new ApiResponse(Boolean.TRUE, "Sukses...", new Gson().toJson(hukdis)), HttpStatus.OK);
        }
        Hukdis save = hukdisRepository.save(hukdis);
        return new ResponseEntity<>(new ApiResponse(Boolean.TRUE, "Sukses...", new Gson().toJson(save)), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse> delete(Long id) {
        hukdisRepository.deleteById(id);
        return new ResponseEntity<>(new ApiResponse(Boolean.TRUE, "Hapus Kasus Sukses...", ""), HttpStatus.OK);
    }
}
