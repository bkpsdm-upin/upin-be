package com.bkd.upin.service.impl;

import com.bkd.upin.model.UnitKerja;
import com.bkd.upin.repository.UnitKerjaRepository;
import com.bkd.upin.service.UnitKerjaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitKerjaServiceImpl implements UnitKerjaService {
    @Autowired
    private UnitKerjaRepository unitKerjaRepository;

    @Override
    public ResponseEntity<List<UnitKerja>> findByStatusAndInscod() {
        List<UnitKerja> list = unitKerjaRepository.findByStatusAndInscodNot("1", "AP");
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}
