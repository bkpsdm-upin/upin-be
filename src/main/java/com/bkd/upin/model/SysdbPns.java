package com.bkd.upin.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class SysdbPns {
    @Id
    @Column(name = "pns_pnsnip")
    private String id;
    private String nipBaru;
    private String pnsFtitle;
    private String pnsRtitle;
    @Column(name = "pns_pnsnam")
    private String nama;
    @Column(name = "pns_pnlcod")
    private String statusHukdis;
    @OneToOne(cascade={CascadeType.DETACH,CascadeType.REFRESH})
    @JoinColumn(name = "pns_wkucod")
    private UnitKerja unitKerja;
    @OneToOne(cascade={CascadeType.DETACH,CascadeType.REFRESH})
    @JoinColumn(name = "pns_crncod")
    private SysdbGolru golongan;
    @OneToOne(cascade={CascadeType.DETACH,CascadeType.REFRESH})
    @JoinColumn(name = "kode_formasi")
    private KodeFormasiJabatan jabatan;
    @Column(name = "idpnsbkn")
    private String idBkn;
}
