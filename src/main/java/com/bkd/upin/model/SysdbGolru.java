package com.bkd.upin.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class SysdbGolru {
    @Id
    @Column(name = "rnk_rnkcod")
    private String id;
    @Column(name = "rnk_rnklbl")
    private String golongan;
    @Column(name = "rnk_rnknam")
    private String pangkat;
    @Column(name = "aturanpph21")
    private String aturan;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGolongan() {
        return golongan;
    }

    public void setGolongan(String golongan) {
        this.golongan = golongan;
    }

    public String getPangkat() {
        return pangkat;
    }

    public void setPangkat(String pangkat) {
        this.pangkat = pangkat;
    }

    public String getAturan() {
        return aturan;
    }

    public void setAturan(String aturan) {
        this.aturan = aturan;
    }
}
