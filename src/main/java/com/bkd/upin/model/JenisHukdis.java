package com.bkd.upin.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "Tabel_Jenishukdis")
public class JenisHukdis {
    @Id
    @Column(name = "kodehukdis")
    private String kodeHukdis;
    private String hukdis;
    @Column(name = "parenthukdis")
    private String parentHukdis;
    @Column(name = "lamahukdis")
    private String lamaHukdis;
    @Column(name = "satuanlamahukdis")
    private  String satuanLamaHukdis;

    public String getKodeHukdis() {
        return kodeHukdis;
    }

    public void setKodeHukdis(String kodeHukdis) {
        this.kodeHukdis = kodeHukdis;
    }

    public String getHukdis() {
        return hukdis;
    }

    public void setHukdis(String hukdis) {
        this.hukdis = hukdis;
    }

    public String getParentHukdis() {
        return parentHukdis;
    }

    public void setParentHukdis(String parentHukdis) {
        this.parentHukdis = parentHukdis;
    }

    public String getLamaHukdis() {
        return lamaHukdis;
    }

    public void setLamaHukdis(String lamaHukdis) {
        this.lamaHukdis = lamaHukdis;
    }

    public String getSatuanLamaHukdis() {
        return satuanLamaHukdis;
    }

    public void setSatuanLamaHukdis(String satuanLamaHukdis) {
        this.satuanLamaHukdis = satuanLamaHukdis;
    }
}
