package com.bkd.upin.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "Tabel_Kelompok_Kasus")
public class TabelKelompokKasus {
    @Id
    @Column(name = "idkelompokkasus")
    private String idKelompokKasus;
    @Column(name = "ppyangmengatur")
    private String ppYangMengatur;

    public String getIdKelompokKasus() {
        return idKelompokKasus;
    }

    public void setIdKelompokKasus(String idKelompokKasus) {
        this.idKelompokKasus = idKelompokKasus;
    }

    public String getPpYangMengatur() {
        return ppYangMengatur;
    }

    public void setPpYangMengatur(String ppYangMengatur) {
        this.ppYangMengatur = ppYangMengatur;
    }
}
