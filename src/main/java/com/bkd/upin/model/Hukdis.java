package com.bkd.upin.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

@Entity
@Data
@Table(name = "Tabel_hukdis")
public class Hukdis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idhukdis")
    private Long idHukdis;
    @Column(name = "idkasus")
    private Long idKasus;
    @Column(name = "niplama")
    private String nipLama;
    @Column(name = "nipbaru")
    private String nip;
    private String nama;
    private String alamat;
    @Column(name = "glrdepan")
    private String glrDepan;
    @Column(name = "glrbelakang")
    private String glrBelakang;
    @NotFound(action = NotFoundAction.IGNORE)
    @OneToOne(cascade={CascadeType.DETACH,CascadeType.REFRESH})
    @JoinColumn(name = "golru")
    private SysdbGolru golru;
    @NotFound(action = NotFoundAction.IGNORE)
    @OneToOne(cascade={CascadeType.DETACH,CascadeType.REFRESH})
    @JoinColumn(name = "unitkerjapns")
    private UnitKerja unitKerja;
    @NotFound(action = NotFoundAction.IGNORE)
    @OneToOne(cascade={CascadeType.DETACH,CascadeType.REFRESH})
    @JoinColumn(name = "jabatan")
    private KodeFormasiJabatan jabatan;
    private String keterangan;
    @NotFound(action = NotFoundAction.IGNORE)
    @OneToOne(cascade={CascadeType.DETACH,CascadeType.REFRESH})
    @JoinColumn(name = "statuspersonal")
    private TabelStatuspersonal statusPersonal;
    @Column(name = "atasanlangsung")
    private String atasanLangsung;
    @Column(name = "pejabatygberwenang")
    private String pejabatYgBerwenang;
    @Column(name = "noskputusan")
    private String noSkPutusan;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "tglskputusan")
    private Date tglSkPutusan;
//    @OneToOne(cascade={CascadeType.DETACH,CascadeType.REFRESH})
    @Column(name = "putusanhukdis")
    private String putusanHukdis;
    @Column(name = "lamahukdisth")
    private String lamaHukdisTh;
    @Column(name = "lamahukdisbl")
    private String lamaHukdisBl;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "berlakudari")
    private Date berlakuDari;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "berlakusampai")
    private Date berlakuSampai;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "tppberlakudari")
    private Date tppBerlakuDari;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "tppberlakusampai")
    private Date tppBerlakuSampai;

    public Long getIdHukdis() {
        return idHukdis;
    }

    public void setIdHukdis(Long idHukdis) {
        this.idHukdis = idHukdis;
    }

    public Long getIdKasus() {
        return idKasus;
    }

    public void setIdKasus(Long idKasus) {
        this.idKasus = idKasus;
    }

    public String getNipLama() {
        return nipLama;
    }

    public void setNipLama(String nipLama) {
        this.nipLama = nipLama;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getGlrDepan() {
        return glrDepan;
    }

    public void setGlrDepan(String glrDepan) {
        this.glrDepan = glrDepan;
    }

    public String getGlrBelakang() {
        return glrBelakang;
    }

    public void setGlrBelakang(String glrBelakang) {
        this.glrBelakang = glrBelakang;
    }

    public SysdbGolru getGolru() {
        return golru;
    }

    public void setGolru(SysdbGolru golru) {
        this.golru = golru;
    }

    public UnitKerja getUnitKerja() {
        return unitKerja;
    }

    public void setUnitKerja(UnitKerja unitKerja) {
        this.unitKerja = unitKerja;
    }

    public KodeFormasiJabatan getJabatan() {
        return jabatan;
    }

    public void setJabatan(KodeFormasiJabatan jabatan) {
        this.jabatan = jabatan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public TabelStatuspersonal getStatusPersonal() {
        return statusPersonal;
    }

    public void setStatusPersonal(TabelStatuspersonal statusPersonal) {
        this.statusPersonal = statusPersonal;
    }

    public String getAtasanLangsung() {
        return atasanLangsung;
    }

    public void setAtasanLangsung(String atasanLangsung) {
        this.atasanLangsung = atasanLangsung;
    }

    public String getPejabatYgBerwenang() {
        return pejabatYgBerwenang;
    }

    public void setPejabatYgBerwenang(String pejabatYgBerwenang) {
        this.pejabatYgBerwenang = pejabatYgBerwenang;
    }

    public String getNoSkPutusan() {
        return noSkPutusan;
    }

    public void setNoSkPutusan(String noSkPutusan) {
        this.noSkPutusan = noSkPutusan;
    }

    public Date getTglSkPutusan() {
        return tglSkPutusan;
    }

    public void setTglSkPutusan(Date tglSkPutusan) {
        this.tglSkPutusan = tglSkPutusan;
    }

    public String getPutusanHukdis() {
        return putusanHukdis;
    }

    public void setPutusanHukdis(String putusanHukdis) {
        this.putusanHukdis = putusanHukdis;
    }

    public String getLamaHukdisTh() {
        return lamaHukdisTh;
    }

    public void setLamaHukdisTh(String lamaHukdisTh) {
        this.lamaHukdisTh = lamaHukdisTh;
    }

    public String getLamaHukdisBl() {
        return lamaHukdisBl;
    }

    public void setLamaHukdisBl(String lamaHukdisBl) {
        this.lamaHukdisBl = lamaHukdisBl;
    }

    public Date getBerlakuDari() {
        return berlakuDari;
    }

    public void setBerlakuDari(Date berlakuDari) {
        this.berlakuDari = berlakuDari;
    }

    public Date getBerlakuSampai() {
        return berlakuSampai;
    }

    public void setBerlakuSampai(Date berlakuSampai) {
        this.berlakuSampai = berlakuSampai;
    }

    public Date getTppBerlakuDari() {
        return tppBerlakuDari;
    }

    public void setTppBerlakuDari(Date tppBerlakuDari) {
        this.tppBerlakuDari = tppBerlakuDari;
    }

    public Date getTppBerlakuSampai() {
        return tppBerlakuSampai;
    }

    public void setTppBerlakuSampai(Date tppBerlakuSampai) {
        this.tppBerlakuSampai = tppBerlakuSampai;
    }
}
