package com.bkd.upin.model.param;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.Date;

@JsonPropertyOrder({"no", "nip", "nama", "pangkat", "golongan", "jabatan", "unitKerja",
        "kelompokKasus", "putusan", "nomorSk", "tanggalSK"})
public class ResponseReportHukdis {
    private Integer no;
    private String nip;
    private String nama;
    private String pangkat;
    private String golongan;
    private String jabatan;
    private String unitKerja;
    private String kelompokKasus;
    private String putusan;
    private String nomorSk;
    @JsonFormat(pattern="dd/MM/yyyy")
    private Date tanggalSk;
    private String keterangan;
    private String status;

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPangkat() {
        return pangkat;
    }

    public void setPangkat(String pangkat) {
        this.pangkat = pangkat;
    }

    public String getGolongan() {
        return golongan;
    }

    public void setGolongan(String golongan) {
        this.golongan = golongan;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getUnitKerja() {
        return unitKerja;
    }

    public void setUnitKerja(String unitKerja) {
        this.unitKerja = unitKerja;
    }

    public String getKelompokKasus() {
        return kelompokKasus;
    }

    public void setKelompokKasus(String kelompokKasus) {
        this.kelompokKasus = kelompokKasus;
    }

    public String getPutusan() {
        return putusan;
    }

    public void setPutusan(String putusan) {
        this.putusan = putusan;
    }

    public String getNomorSk() {
        return nomorSk;
    }

    public void setNomorSk(String nomorSk) {
        this.nomorSk = nomorSk;
    }

    public Date getTanggalSk() {
        return tanggalSk;
    }

    public void setTanggalSk(Date tanggalSk) {
        this.tanggalSk = tanggalSk;
    }


    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
