package com.bkd.upin.model.param;

import com.bkd.upin.model.TabelKasus;
import com.bkd.upin.utils.AppConstants;

public class PagingRequestBody {
    private Integer page;
    private Integer size;
    private TabelKasus kasus;

    public Integer getPage() {
        return page == null ? Integer.valueOf(AppConstants.DEFAULT_PAGE_NUMBER) : page;
    }

    public Integer getSize() {
        return size == null ? Integer.valueOf(AppConstants.DEFAULT_PAGE_SIZE) : size;
    }

    public TabelKasus getKasus() {
        return kasus;
    }

}
