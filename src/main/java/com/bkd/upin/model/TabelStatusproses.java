package com.bkd.upin.model;

import lombok.Data;
import javax.persistence.*;

@Entity
@Data
public class TabelStatusproses {
    @Id
    @Column(name = "idstatusproses")
    private String idStatusProses;
    @Column(name = "statusproses")
    private String statusProses;

    public String getIdStatusProses() {
        return idStatusProses;
    }

    public void setIdStatusProses(String idStatusProses) {
        this.idStatusProses = idStatusProses;
    }

    public String getStatusProses() {
        return statusProses;
    }

    public void setStatusProses(String statusProses) {
        this.statusProses = statusProses;
    }
}
