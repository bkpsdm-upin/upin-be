package com.bkd.upin.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "SYSDB_WKGUNI")
public class UnitKerja {
    @Id
    @Column(name = "wku_wrkcod")
    private String wkuWrkcod;
    @Column(name = "wku_ukerpjg")
    private String nama;
    @Column(name = "wku_inscod")
    private String inscod;
    @Column(name = "wku_alamat")
    private String alamat;
    private String status;

    public String getWkuWrkcod() {
        return wkuWrkcod;
    }

    public void setWkuWrkcod(String wkuWrkcod) {
        this.wkuWrkcod = wkuWrkcod;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getInscod() {
        return inscod;
    }

    public void setInscod(String inscod) {
        this.inscod = inscod;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
