package com.bkd.upin.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class TabelKasus {
    public static final String ID_KASUS_FIELD = "idkasus";
    public static final String ASAL_FIELD = "asal";
    public static final String UNIT_KERJA_FIELD = "unitkerja";
    public static final String ALAMAT_FIELD = "alamat";
    public static final String NO_SURAT_ADUAN_FIELD = "nosurataduan";
    public static final String TGL_SURAT_ADUAN_FIELD = "tanggalSuratAduan";
    public static final String ISI_SURAT_FIELD = "isisurat";
    public static final String KETERANGAN_FIELD = "keterangan";
    public static final String KELOMPOK_KASUS_FIELD = "kelompokKasus";
    public static final String STATUS_PROSES_FIELD = "statusProses";
    public static final String STATUS_VERIFIKASI_FIELD = "statusVerifikasi";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idkasus")
    private Long idKasus;
    private String asal;
    @Column(name = "unitkerja")
    private String unitKerja;
    private String alamat;
    @Column(name = "nosurataduan")
    private String noSuratAduan;
    @Column(name = "tanggalsurataduan")
    private Date tanggalSuratAduan;
    @Column(name = "isisurat")
    private String isiSurat;
    private String keterangan;
    @OneToOne(cascade={CascadeType.DETACH,CascadeType.REFRESH})
    @JoinColumn(name = "kelompokkasus")
    private TabelKelompokKasus kelompokKasus;
    @OneToOne(cascade={CascadeType.DETACH,CascadeType.REFRESH})
    @JoinColumn(name = "statusproses")
    private TabelStatusproses statusProses;
    private Integer statusVerifikasi;
    private Date createdAt;
    private String createdBy;
    private Date verifiedAt;
    private String verifiedBy;


    public Long getIdKasus() {
        return idKasus;
    }

    public void setIdKasus(Long idKasus) {
        this.idKasus = idKasus;
    }

    public String getAsal() {
        return asal;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public String getUnitKerja() {
        return unitKerja;
    }

    public void setUnitKerja(String unitKerja) {
        this.unitKerja = unitKerja;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoSuratAduan() {
        return noSuratAduan;
    }

    public void setNoSuratAduan(String noSuratAduan) {
        this.noSuratAduan = noSuratAduan;
    }

    public Date getTanggalSuratAduan() {
        return tanggalSuratAduan;
    }

    public void setTanggalSuratAduan(Date tanggalSuratAduan) {
        this.tanggalSuratAduan = tanggalSuratAduan;
    }

    public String getIsiSurat() {
        return isiSurat;
    }

    public void setIsiSurat(String isiSurat) {
        this.isiSurat = isiSurat;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public TabelKelompokKasus getKelompokKasus() {
        return kelompokKasus;
    }

    public void setKelompokKasus(TabelKelompokKasus kelompokKasus) {
        this.kelompokKasus = kelompokKasus;
    }

    public TabelStatusproses getStatusProses() {
        return statusProses;
    }

    public void setStatusProses(TabelStatusproses statusProses) {
        this.statusProses = statusProses;
    }

    public Integer getStatusVerifikasi() {
        return statusVerifikasi;
    }

    public void setStatusVerifikasi(Integer statusVerifikasi) {
        this.statusVerifikasi = statusVerifikasi;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getVerifiedAt() {
        return verifiedAt;
    }

    public void setVerifiedAt(Date verifiedAt) {
        this.verifiedAt = verifiedAt;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }
}
