package com.bkd.upin.repository;

import com.bkd.upin.model.SysdbPns;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<SysdbPns, String> {
    SysdbPns findByNipBaru(String nip);
    @Modifying
    @Transactional
    @Query(value = "UPDATE SYSDB_PNS SET PNS_PNLCOD = :kode where NIP_BARU = :nip", nativeQuery = true)
    void update(@Param("nip") Long nip, @Param("kode") String kode);
}
