package com.bkd.upin.repository;

import com.bkd.upin.model.TabelKasus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface KasusRepository extends JpaRepository<TabelKasus, Long>, JpaSpecificationExecutor<TabelKasus> {
//    Page<TabelKasus> findByStatusVerifikasi(Integer status, Pageable pageable);
}
