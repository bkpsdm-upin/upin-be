package com.bkd.upin.repository;

import com.bkd.upin.model.JenisHukdis;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JenisHukdisRepository extends JpaRepository<JenisHukdis, String> {
    Optional<JenisHukdis> findByKodeHukdis(String kodeHukdis);
}
