package com.bkd.upin.repository;

import com.bkd.upin.model.TabelKelompokKasus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KelompokKasusRepository extends JpaRepository<TabelKelompokKasus, String> {
    List<TabelKelompokKasus> findAll();
}
