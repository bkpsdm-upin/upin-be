package com.bkd.upin.repository.spesification;

import com.bkd.upin.model.TabelKasus;
import com.bkd.upin.model.TabelKelompokKasus;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

public class KasusSpesifications {

    public static Specification<TabelKasus> findBySearchCreiteria(TabelKasus search) {
        return new Specification<TabelKasus>() {
            @Override
            public Predicate toPredicate(Root<TabelKasus> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                final Collection<Predicate> predicates = new ArrayList<>();

                if(!StringUtils.isEmpty(search.getStatusVerifikasi())) {
                    final Predicate predicate = criteriaBuilder.equal(root.get(TabelKasus.STATUS_VERIFIKASI_FIELD), search.getStatusVerifikasi());
                    predicates.add(predicate);
                } else {
                    final Predicate predicate = criteriaBuilder.isNull(root.get(TabelKasus.STATUS_VERIFIKASI_FIELD));
                    predicates.add(predicate);
                }

                if(!StringUtils.isEmpty(search.getAsal())) {
                    final Predicate predicate = criteriaBuilder.like(root.get(TabelKasus.ASAL_FIELD), "%"+search.getAsal()+"%");
                    predicates.add(predicate);
                }
                if(search.getKelompokKasus() != null) {
                    final Predicate predicate = criteriaBuilder.equal(root.get(TabelKasus.KELOMPOK_KASUS_FIELD), search.getKelompokKasus());
                    predicates.add(predicate);
                }
                if(search.getStatusProses() != null) {
                    final Predicate predicate = criteriaBuilder.equal(root.get(TabelKasus.STATUS_PROSES_FIELD), search.getStatusProses());
                    predicates.add(predicate);
                }
                if(!StringUtils.isEmpty(search.getTanggalSuratAduan())) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(search.getTanggalSuratAduan());
                    int year = calendar.get(Calendar.YEAR);

                    final Predicate predicate = criteriaBuilder.equal(criteriaBuilder.function("YEAR", Integer.class, root.get(TabelKasus.TGL_SURAT_ADUAN_FIELD)), year);
                    predicates.add(predicate);
                }

                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
    }
}
