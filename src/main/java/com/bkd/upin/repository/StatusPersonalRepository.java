package com.bkd.upin.repository;

import com.bkd.upin.model.TabelStatuspersonal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusPersonalRepository extends JpaRepository<TabelStatuspersonal, String> {
}
