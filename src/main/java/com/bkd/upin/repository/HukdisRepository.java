package com.bkd.upin.repository;

import com.bkd.upin.model.Hukdis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface HukdisRepository extends JpaRepository<Hukdis, Long> {
    List<Hukdis> findAllByIdKasus(Long idKasus);
    List<Hukdis> findAllByNipLamaNotNull();

    @Modifying
    @Query(value = "insert into Tabel_Hukdis (IDKasus,nama,alamat,keterangan,StatusPersonal) VALUES (:idKasus,:nama,:alamat,:keterangan,:statusPersonal)", nativeQuery = true)
    @Transactional
    void insert(@Param("idKasus") Long idKasus,
                @Param("nama") String nama,
                @Param("alamat") String alamat,
                @Param("keterangan") String keterangan,
                @Param("statusPersonal") String statusPersonal);
}
