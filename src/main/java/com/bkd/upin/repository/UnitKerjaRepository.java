package com.bkd.upin.repository;

import com.bkd.upin.model.UnitKerja;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UnitKerjaRepository extends JpaRepository<UnitKerja, String> {
    List<UnitKerja> findByStatusAndInscodNot(String status, String Inscod);
}
