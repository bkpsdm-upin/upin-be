package com.bkd.upin.repository;

import com.bkd.upin.model.TabelStatusproses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusProsesRepository extends JpaRepository<TabelStatusproses, String> {
}
