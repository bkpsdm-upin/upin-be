package com.bkd.upin.controller;

import com.bkd.upin.exception.ResponseEntityErrorException;
import com.bkd.upin.model.Hukdis;
import com.bkd.upin.payload.ApiResponse;
import com.bkd.upin.payload.PagedResponse;
import com.bkd.upin.service.HukdisService;
import com.bkd.upin.utils.AppConstants;
import com.bkd.upin.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/hukdis")
public class HukdisController {

    @Autowired
    private HukdisService hukdisService;

    @ExceptionHandler(ResponseEntityErrorException.class)
    public ResponseEntity<ApiResponse> handleExceptions(ResponseEntityErrorException exception) {
        return exception.getApiResponse();
    }

    @GetMapping("/jenisHukdis")
    public ResponseEntity<Map<String, Object>> jenisHukdis() {
        return hukdisService.jenisHukdis();
    }

    @GetMapping
    public PagedResponse<Hukdis> allHukdis(
            @RequestParam(name = "page", required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(name = "size", required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        AppUtils.validatePageNumberAndSize(page, size);

        return hukdisService.allHukdis(page, size);
    }

    @GetMapping("/{idKasus}")
    public ResponseEntity<List<Hukdis>> allHukdisByIdKasus(@Valid @PathVariable(name = "idKasus") Long idKasus) {

        return hukdisService.allHukdisByIdKasus(idKasus);
    }

    @GetMapping("/detail/{idKasus}")
    public ResponseEntity<ApiResponse> detailHukdis(@Valid @PathVariable(name = "idKasus") Long idKasus) {

        return hukdisService.listHukdis(idKasus);
    }

    @PutMapping()
    public ResponseEntity<ApiResponse> updateHukdis(@Valid @RequestBody Hukdis hukdis) {
        return hukdisService.updateHukdis(hukdis);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteHukdis(@Valid @PathVariable(name = "id") Long id) {
        return hukdisService.delete(id);
    }

}
