package com.bkd.upin.controller;

import com.bkd.upin.exception.ResponseEntityErrorException;
import com.bkd.upin.model.UnitKerja;
import com.bkd.upin.payload.ApiResponse;
import com.bkd.upin.service.UnitKerjaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/unitKerja")
public class UnitKerjaController {

    @Autowired
    private UnitKerjaService unitKerjaService;

    @ExceptionHandler(ResponseEntityErrorException.class)
    public ResponseEntity<ApiResponse> handleExceptions(ResponseEntityErrorException exception) {
        return exception.getApiResponse();
    }

    @GetMapping
    public ResponseEntity<List<UnitKerja>> unitKerja() {
        return unitKerjaService.findByStatusAndInscod();
    }
}
