package com.bkd.upin.controller;

import com.bkd.upin.exception.ResponseEntityErrorException;
import com.bkd.upin.model.SysdbPns;
import com.bkd.upin.payload.ApiResponse;
import com.bkd.upin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @ExceptionHandler(ResponseEntityErrorException.class)
    public ResponseEntity<ApiResponse> handleExceptions(ResponseEntityErrorException exception) {
        return exception.getApiResponse();
    }

    @GetMapping("/{nip}")
    public ResponseEntity<ApiResponse> userByNip(@PathVariable(name = "nip") String nip) {
        return userService.getByNip(nip);
    }

    @PutMapping("/updateRiwayatHukdis/{nip}/{kode}")
    public ResponseEntity<ApiResponse> updateRiwayatHukdis(@PathVariable(name = "nip") Long nip,
                                                           @PathVariable(name = "kode") String kode) {
        return userService.setRiwayatHukdis(nip, kode);
    }

}
