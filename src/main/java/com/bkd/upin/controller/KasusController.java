package com.bkd.upin.controller;

import com.bkd.upin.exception.ResponseEntityErrorException;
import com.bkd.upin.model.TabelKasus;
import com.bkd.upin.model.TabelKelompokKasus;
import com.bkd.upin.model.TabelStatuspersonal;
import com.bkd.upin.model.TabelStatusproses;
import com.bkd.upin.model.param.PagingRequestBody;
import com.bkd.upin.model.param.ResponseReportHukdis;
import com.bkd.upin.payload.ApiResponse;
import com.bkd.upin.payload.PagedResponse;
import com.bkd.upin.service.KasusService;
import com.bkd.upin.utils.AppConstants;
import com.bkd.upin.utils.AppUtils;
import com.google.gson.JsonObject;
import net.minidev.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/kasus")
public class KasusController {

    @Autowired
    private KasusService kasusService;

    @ExceptionHandler(ResponseEntityErrorException.class)
    public ResponseEntity<ApiResponse> handleExceptions(ResponseEntityErrorException exception) {
        return exception.getApiResponse();
    }

    @PostMapping
    public PagedResponse<TabelKasus> kasus(
            @Valid @RequestBody PagingRequestBody requestBody) {
        int page = requestBody.getPage();
        int size = requestBody.getSize();
        TabelKasus kasus = requestBody.getKasus() == null ? new TabelKasus() : requestBody.getKasus();
        AppUtils.validatePageNumberAndSize(page, size);

        return kasusService.kasuses(kasus, page, size);
    }

    @PostMapping("/report")
    public ResponseEntity<List<ResponseReportHukdis>> reportKasus(@Valid @RequestBody TabelKasus kasus) {
        return kasusService.reporting(kasus);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TabelKasus> getKasus(@PathVariable(name = "id") Long id) {
        return kasusService.getKasus(id);
    }

    @PutMapping()
//    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<ApiResponse> updateKasus(@Valid @RequestBody TabelKasus newKasus) {
        Long id = null;
        return kasusService.updateKasus(id, newKasus);
    }

    @PostMapping("/verifikasi")
    public ResponseEntity<ApiResponse> verifikasiKasus(@Valid @RequestBody TabelKasus kasus) {
        return kasusService.verifikasiKasus(kasus);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteKasus(@PathVariable(name = "id") Long id) {
//        return kasusService.deleteKasus(id);
        return null;
    }

    @GetMapping("/kelompok")
    public PagedResponse<TabelKelompokKasus> groupKasus(
            @RequestParam(name = "page", required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(name = "size", required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        AppUtils.validatePageNumberAndSize(page, size);

        return kasusService.kelompokKasus(page, size);
    }

    @GetMapping("/proses")
    public PagedResponse<TabelStatusproses> prosesKasus(
            @RequestParam(name = "page", required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(name = "size", required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        AppUtils.validatePageNumberAndSize(page, size);

        return kasusService.statusProses(page, size);
    }

    @GetMapping("/statusPersonal")
    public PagedResponse<TabelStatuspersonal> statusPersonal(
            @RequestParam(name = "page", required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(name = "size", required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        AppUtils.validatePageNumberAndSize(page, size);

        return kasusService.statusPersonal(page, size);
    }


}
